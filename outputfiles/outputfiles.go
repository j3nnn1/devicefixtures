package outputfiles

import (
	"encoding/csv"
	"fmt"
	"gitlab.intraway.com/jennifer.maldonado/devicefixtures/fixture"
	"os"
)

var Writer *csv.Writer
var SqlWriter *csv.Writer
var SqlWriterR *csv.Writer

var CsvOutput *os.File
var SqlOutput *os.File
var SqlRollbackOutput *os.File
var err error

func InitializeFileHandler() {
	CsvOutput, err := os.Create("jmeterThreadConfig.csv")
	SqlOutput, err = os.Create("jmeterThreadConfig.sql")
	SqlRollbackOutput, err := os.Create("jmeterThreadConfigRollback.sql")

	if err != nil {
		fmt.Println("An error encountered ::", err)
	}
	Writer = csv.NewWriter(CsvOutput)
	SqlWriterR = csv.NewWriter(SqlRollbackOutput)

}
func Flush() {
	defer Writer.Flush()
	defer SqlWriterR.Flush()
	defer SqlRollbackOutput.Close()
	defer CsvOutput.Close()
}
func SaveStateTagToFile(device fixture.Device, stateTag fixture.STATE_TAGS) {
	// INSERT INTO t1 (a,b,c) VALUES (1,2,3),(4,5,6) AS new(m,n,p)
	//  ON DUPLICATE KEY UPDATE c = m+n;
	sqlLineStateTag := fmt.Sprintf("insert into state_tags(spid, st_name) values (%d, \"%s\") ON DUPLICATE KEY UPDATE stid=stid;", device.ServiceProviderId, device.StateTagName)
	fmt.Fprintln(SqlOutput, sqlLineStateTag)
	fmt.Fprintln(SqlOutput, "commit;")
	sqlLineStateTagRollback := fmt.Sprintf("delete from state_tags where stid=%d and st_name='%s' and spid='%d';", stateTag.STID, stateTag.STNAME, stateTag.SPID)
	SqlWriterR.Write([]string{sqlLineStateTagRollback})
	SqlWriterR.Write([]string{"commit;"})
}
func SaveDeviceStateToFile(device fixture.Device, devicestate fixture.DEVICESTATE) {
	deviceStateJson := fmt.Sprintf("'{\\\"rack\\\":\\\"%s\\\",\\\"shelf\\\":\\\"%s\\\",\\\"slot\\\":\\\"%s\\\",\\\"port\\\":\\\"%s\\\",\\\"state\\\":\\\"%s\\\",\\\"clientID\\\":\\\"%s\\\",\\\"clientName\\\":\\\"%s\\\",\\\"organizationCode\\\":\\\"%s\\\",\\\"VLAN_SP_PAIR\\\":[{\\\"serviceProvider\\\":\\\"%s\\\",\\\"vlanID\\\":\\\"%s\\\"}]}'", device.Rack, device.Shelf, device.Slot, device.Port,  device.StateTagName, device.ClientId, device.ClientName, device.OrganizationCode, device.ServiceProviderName, device.VlanId)
	sqlLineDeviceState := fmt.Sprintf("insert into device_state(serial_number, state_json) values (\"%s\", \"%s\") ON DUPLICATE KEY UPDATE serial_number=serial_number;", device.Serialnumber, deviceStateJson)
	fmt.Fprintln(SqlOutput, sqlLineDeviceState)
	fmt.Fprintln(SqlOutput, "commit;")
	sqlLineDeviceStateRollback := fmt.Sprintf("delete from device_state where serial_number=\"%s\";", devicestate.SERIALNUMBER)
	SqlWriterR.Write([]string{sqlLineDeviceStateRollback})
	SqlWriterR.Write([]string{"commit;"})
}
func SaveCircuitIdCsvFile(device fixture.Device) {
	value := []string{device.Rack, device.Shelf, device.Slot, device.Port, device.VlanId, device.ServiceProviderName, device.StateTagName, device.ClientName, device.ClientId, device.OrganizationCode, device.Serialnumber, device.Macaddress, device.CircuitId, device.IPV4}
	Writer.Write(value)
}
