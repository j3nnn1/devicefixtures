package sqlconnector

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	fixtureConfig "gitlab.intraway.com/jennifer.maldonado/devicefixtures/config"
	logger "gitlab.intraway.com/jennifer.maldonado/devicefixtures/logger"
)

var DB *gorm.DB

func InitSQL() error {
	var err error
	username := fixtureConfig.ConfMap.MysqlUser
	password := fixtureConfig.ConfMap.MysqlPassword
	dirAndPort := fixtureConfig.ConfMap.MysqlDirAndPort
	database := fixtureConfig.ConfMap.DataBase
	connection := username + ":" + password + "@" + "tcp(" + dirAndPort + ")/" + database + "?charset=utf8&parseTime=true"
	logger.Log.Debug("Error MySQL connection : ", connection)
	DB, err = gorm.Open("mysql", connection)
	if err != nil {
		logger.Log.Errorf("Error MySQL connection : %s", err.Error())
		return err
	}
	return nil
}

type GormWrapper struct {
	sqlConn *gorm.DB
}

func (g *GormWrapper) Build() GormOrmInterface {
	g.sqlConn = DB
	return g
}

func (g *GormWrapper) GetRowsAffected() int64 {
	return g.sqlConn.RowsAffected
}
func (g *GormWrapper) Error() error {
	return g.Error()
}
func (g *GormWrapper) RecordNotFound() bool {
	return g.sqlConn.RecordNotFound()
}

func (g *GormWrapper) Find(out interface{}, where ...interface{}) GormOrmInterface {
	g.sqlConn.Find(out, where)
	return g
}

func (g *GormWrapper) Create(value interface{}) GormOrmInterface {
	g.sqlConn.Create(value)
	return g
}

func (g *GormWrapper) Model(value interface{}) GormOrmInterface {
	g.sqlConn.Model(value)
	return g
}
func (g *GormWrapper) Update(attrs ...interface{}) GormOrmInterface {
	g.sqlConn.Update(attrs)
	return g
}
func (g *GormWrapper) First(out interface{}, where ...interface{}) GormOrmInterface {
	g.sqlConn.First(out, where)
	return g
}

func (g *GormWrapper) Delete(value interface{}, where ...interface{}) GormOrmInterface {
	g.sqlConn.Delete(value, where)
	return g
}
func (g *GormWrapper) Save(value interface{}) GormOrmInterface {
	g.sqlConn.Save(value)
	return g
}
func (g *GormWrapper) Where(query interface{}, args ...interface{}) GormOrmInterface {
	g.sqlConn.Where(query, args)
	return g
}

type GormOrmInterface interface {
	GetRowsAffected() int64
	RecordNotFound() bool
	Error() error
	// Build() GormOrmInterface
	Find(out interface{}, where ...interface{}) GormOrmInterface
	Create(value interface{}) GormOrmInterface
	Model(value interface{}) GormOrmInterface
	Update(attrs ...interface{}) GormOrmInterface
	First(out interface{}, where ...interface{}) GormOrmInterface
	Delete(value interface{}, where ...interface{}) GormOrmInterface
	Save(value interface{}) GormOrmInterface
	Where(query interface{}, args ...interface{}) GormOrmInterface
}

var _ GormOrmInterface = (*GormWrapper)(nil)
