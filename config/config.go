package config

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/davecgh/go-spew/spew"
	"github.com/fsnotify/fsnotify"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// Configuration estructura
type Configuration struct {
	OutputPath                      string `mapstructure:"devicesfixture_outputpath"`
	LoggingPath                      string `mapstructure:"devicesfixture_logpath"`
	LoggingFile                      string `mapstructure:"devicesfixture_logfile"`
	LoggingLevel                     string `mapstructure:"devicesfixture_loglevel"`
	// entradas de conexion MySQL
	MysqlUser       string `mapstructure:"devicesfixture_mysql_username"`
	MysqlPassword   string `mapstructure:"devicesfixture_mysql_password"`
	MysqlDirAndPort string `mapstructure:"devicesfixture_mysql_dir_and_port"`
	DataBase        string `mapstructure:"devicesfixture_mysql_database"`
}

// Config is package struct containing conf params
var ConfMap Configuration

func Load(path string, name string, ext string) {

	// name := "parameters"
	// ext := "yml"
	// path := "./config"
	fmt.Printf("Loading configuration %s/%s.%s\n", path, name, ext)
	viper.SetConfigType(ext)
	viper.SetConfigName(name)
	viper.AddConfigPath(path)

	// Setting defaults
	viper.SetDefault("devicesfixture_storage_mysql_dbname", "root")
	viper.SetDefault("devicesfixture_storage_mysql_username", "intraway")
	viper.SetDefault("devicesfixture_storage_mysql_password", "iwjaja")
	viper.SetDefault("devicesfixture_mysql_dir_and_port", "127.0.0.1:3306")
	viper.SetDefault("devicesfixture_outputpath", "./output/")

	if _, err := os.Stat(filepath.Join(path, name+"."+ext)); err == nil {
		err = viper.ReadInConfig()
		if err == nil {
			viper.WatchConfig()

			viper.OnConfigChange(func(e fsnotify.Event) {
				log.Println("Config file changed: ", e.Name)
			})
		} else {
			log.Errorln(err)
		}
	} else {
		log.Warningf("File parameters.yml not found. Working with default config: %s \n", err)
	}

	//var _conf Configuration
	err := viper.Unmarshal(&ConfMap)
	if err != nil {
		log.Fatalf("Unable to decode into struct, %+v", err)
	}
	fmt.Printf("Load configuration : \n")
	spew.Dump(ConfMap)
	//return _conf
}
