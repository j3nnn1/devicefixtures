module gitlab.intraway.com/jennifer.maldonado/devicefixtures

go 1.14

require (
	github.com/corpix/uarand v0.1.1 // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-sql-driver/mysql v1.5.0
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/jinzhu/gorm v1.9.16
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.1
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
)
