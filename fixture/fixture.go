package fixture

import (
	"fmt"
	"github.com/icrowley/fake"
	_ "github.com/icrowley/fake"
	"gitlab.intraway.com/jennifer.maldonado/devicefixtures/logger"
	dbcli "gitlab.intraway.com/jennifer.maldonado/devicefixtures/sqlconnector"
	"log"
	"math/rand"
	"time"
)

var states[] STATE_TAGS
var policies[] ServiceProvider
var macLetters = []rune("0123456789abcdef")
const organizationCode = "TIGOPY"


type ServiceProvider struct {
	SpName string
	Vlan   string
	SERVICEPROVIDERID int
}
type DEVICESTATE struct {
	SERIALNUMBER string `gorm:"column:SERIAL_NUMBER;primary_key"`
	STATEJSON    string `gorm:"column:STATE_JSON"`
}
// TableName sets the insert table name for this struct type
func (d *DEVICESTATE) TableName() string {
	return "DEVICE_STATE"
}

type STATE_TAGS struct {
	STID   int    `gorm:"column:STID;primary_key"`
	STNAME string `gorm:"column:ST_NAME"`
	SPID   int    `gorm:"column:SPID"`
}

// cada linea es un registro para un thread/ usuario concurrente.
// rack, shelf, slot, port, state, serviceProvider, vlanID, clientID, clientName, organizationCode, serialnumber, macaddress, CircuitID
// 0,0,0,15,"OPERATING","OPERATING",3422,9999999999,"Jennifer","TIGOPY","48575443C7D5639A","FF:FF:FF:FF:FF:FF", "OLT1.LBS1 xpon 0/0/15/1:3422.0.48575443C7D5639A"

type Device struct {
	Rack         string
	Shelf        string
	Slot         string
	Port         string
	ClientId     string
	ClientName   string
	Serialnumber string
	Macaddress   string

	VlanId              string
	ServiceProviderName string
	ServiceProviderId   int
	StateTagName        string
	OrganizationCode    string
	CircuitId           string
	IPV4 string
}

func GetDevice() Device {
	var newDevice Device

	newDevice.Rack = fake.Digits()
	newDevice.Shelf = fake.Digits()
	newDevice.Slot = fake.DigitsN(2)
	newDevice.Port = fake.DigitsN(2)
	newDevice.ClientName = fake.FirstName()+" "+fake.LastName()
	newDevice.ClientId = fake.DigitsN(10)
	newDevice.Macaddress = randSeq(12)
	newDevice.Serialnumber = randSeq(16)
	newDevice.IPV4 = fake.IPv4()

	totalStates := len(states)
	totalPolicies := len(policies)

	if totalStates <= 0 {
		log.Fatal("watch out!, State tags table has no rows!!")
	}

	rand.Seed(time.Now().UnixNano())
	idxPolicy := rand.Intn(totalPolicies)

	rand.Seed(time.Now().UnixNano())
	idxState := rand.Intn(totalStates)


	sp := policies[idxPolicy]
	st := states[idxState]

	newDevice.VlanId = sp.Vlan
	newDevice.ServiceProviderName = sp.SpName
	newDevice.ServiceProviderId = sp.SERVICEPROVIDERID
	newDevice.StateTagName = st.STNAME
	newDevice.OrganizationCode = organizationCode
	newDevice.CircuitId = fmt.Sprintf("\"OLT1.LBS1 xpon " +newDevice.Rack +"/"+newDevice.Shelf +"/"+newDevice.Port +"/"+newDevice.Slot +":"+newDevice.VlanId +".0."+newDevice.Serialnumber+"\"")
	return newDevice
}
func LoadServiceProviders() {
	logger.Log.Debug("Getting Service Providers from database");
	var sp ServiceProvider
	rows, _ := dbcli.DB.Model(&sp).Raw("SELECT  `sp_name`, `vlan`, `serviceproviderid` FROM dhcplogic.SERVICE_PROVIDERS;").Rows()
	//fmt.Println(rows)
	defer rows.Close()
	for rows.Next() {
		//dbcli.DB.ScanRows(rows, &sp)
		rows.Scan(&sp.SpName, &sp.Vlan, &sp.SERVICEPROVIDERID)
		policies = append(policies, sp)
		logger.Log.Debug("result SP_NAME: "+  sp.SpName + " result VLAN: "+ sp.Vlan)
		//fmt.Println(sp)
	}
}
func LoadStateTags() {
	logger.Log.Debug("Getting States from database");
	var state STATE_TAGS
	rows1, _ := dbcli.DB.Model(&state).Raw("SELECT stid, st_name, spid FROM dhcplogic.STATE_TAGS;").Rows()
	defer rows1.Close()
	for rows1.Next() {
		rows1.Scan(&state.STID, &state.STNAME, &state.SPID)
		states = append(states, state)
		logger.Log.Debug("result StateTag: " +  state.STNAME)
		//fmt.Println(state)
	}
}

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = macLetters[rand.Intn(len(macLetters))]
	}
	return string(b)
}