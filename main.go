package main

import (
	"fmt"
	_ "github.com/icrowley/fake"
	fixtureConfig "gitlab.intraway.com/jennifer.maldonado/devicefixtures/config"
	"gitlab.intraway.com/jennifer.maldonado/devicefixtures/fixture"
	logger "gitlab.intraway.com/jennifer.maldonado/devicefixtures/logger"
	"gitlab.intraway.com/jennifer.maldonado/devicefixtures/outputfiles"
	dbcli "gitlab.intraway.com/jennifer.maldonado/devicefixtures/sqlconnector"
)
type ServiceProvider struct {
	SpName string
	Vlan   string
	SERVICEPROVIDERID int
}
type DEVICESTATE struct {
	SERIALNUMBER string `gorm:"column:SERIAL_NUMBER;primary_key"`
	STATEJSON    string `gorm:"column:STATE_JSON"`
}
func (d *DEVICESTATE) TableName() string {
	return "DEVICE_STATE"
}


func main() {
	fixtureConfig.Load("./config", "parameters", "yml")
	logger.IntLog()
	dbcli.InitSQL()
	fixture.LoadServiceProviders()
	fixture.LoadStateTags()
	logger.Log.Debug("Making SQL inserts to Mysql");

	outputfiles.InitializeFileHandler()

	quantity := 5
	for i:=1; i<=quantity; i++ {
		newDevice := fixture.GetDevice()

		outputfiles.SaveCircuitIdCsvFile(newDevice)

		var stateTag fixture.STATE_TAGS
		var deviceStateRow fixture.DEVICESTATE

		// insert en state tags. necesito el service provider id
		dbcli.DB.Model(&fixture.STATE_TAGS{}).Where(map[string]interface{}{"ST_NAME": newDevice.StateTagName, "SPID": newDevice.ServiceProviderId}).FirstOrCreate(&stateTag)
		outputfiles.SaveStateTagToFile(newDevice, stateTag)

		// insert de device state
		//{"rack":"0","shelf":"0","slot":"15","port":"1","state":"OPERATING","clientID":"CLI-1002","clientName":"Rick Hunter","organizationCode":"TECOAR","VLAN_SP_PAIR":[{"serviceProvider":"OPERATING","vlanID":"3422"}]}
		deviceStateRow.SERIALNUMBER = newDevice.Serialnumber
		deviceStateRow.STATEJSON = fmt.Sprintf("{\"rack\":\"%s\",\"shelf\":\"%s\",\"slot\":\"%s\",\"port\":\"%s\",\"state\":\"%s\",\"clientID\":\"%s\",\"clientName\":\"%s\",\"organizationCode\":\"%s\",\"VLAN_SP_PAIR\":[{\"serviceProvider\":\"%s\",\"vlanID\":\"%s\"}]}", newDevice.Rack, newDevice.Shelf, newDevice.Slot, newDevice.Port,  newDevice.StateTagName, newDevice.ClientId, newDevice.ClientName, newDevice.OrganizationCode, newDevice.ServiceProviderName, newDevice.VlanId)

		dbcli.DB.Model(&fixture.DEVICESTATE{}).Where(map[string]interface{}{"SERIAL_NUMBER": newDevice.Serialnumber}).FirstOrCreate(&deviceStateRow)
		outputfiles.SaveDeviceStateToFile(newDevice, deviceStateRow)
	}

	outputfiles.SqlOutput.Close()
	outputfiles.Flush()
}
