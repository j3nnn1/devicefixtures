package logger

import (
	"fmt"
	"os"
	"path/filepath"
	"time"
	//	"net"

	"github.com/sirupsen/logrus"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
	fixtureConfig "gitlab.intraway.com/jennifer.maldonado/devicefixtures/config"
)

var Log *logrus.Logger

func IntLog() {
	Log = logrus.New()
	// Creating log dir if not exists
	if _, err := os.Stat(fixtureConfig.ConfMap.LoggingPath); os.IsNotExist(err) {
		if err = os.MkdirAll(fixtureConfig.ConfMap.LoggingPath, 0777); err != nil {
			if os.IsPermission(err) {
				fmt.Println("Try fix the permission issue, by creating the dir structure and try again.")
				panic(err)
			}
		}
	}
	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example

	f := filepath.Join(fixtureConfig.ConfMap.LoggingPath, fixtureConfig.ConfMap.LoggingFile)
	fmt.Printf("Logging on : %s\n", f)
	file, err := os.OpenFile(f, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
	if err == nil {
		Log.SetOutput(file)
	} else {
		fmt.Println("Failed to log to file, using default stderr : ", err)
		Log.SetOutput(os.Stderr)
	}
	Log.SetOutput(file)
	// Log as JSON instead of the default ASCII formatter.
	//Log.SetFormatter(prefixed.TextFormatter)
	Log.Formatter = new(prefixed.TextFormatter)
	Log.Formatter.(*prefixed.TextFormatter).ForceFormatting = true
	Log.Formatter.(*prefixed.TextFormatter).FullTimestamp = true

	// Only log the warning severity or above.
	lvl, err := logrus.ParseLevel(fixtureConfig.ConfMap.LoggingLevel)
	if err != nil {
		lvl = logrus.InfoLevel
	}
	Log.SetLevel(lvl)
}

func makeTimestamp() int64 {
	return time.Now().UnixNano() / (int64(time.Millisecond) / int64(time.Nanosecond))
}

type LogInterface interface {
	Debug(args ...interface{})
	Info(args ...interface{})
	Error(args ...interface{})
	Fatal(args ...interface{})
	Tracef(format string, args ...interface{})
	Debugf(format string, args ...interface{})
	Infof(format string, args ...interface{})
	Errorf(format string, args ...interface{})
	Fatalf(format string, args ...interface{})
}

var _ LogInterface = (*logrus.Logger)(nil) // (*logrus.Logger) implements LogInterface

// Format to use
// {
// 	@timestamp: date(ISO8601),
// 	%date{yyyy-MM-dd'T'HH:mm:ss.SSS, UTC},
// 	level: WARN/INFO/ERROR/DEBUG/TRACE, OK
// 	host: nombre del host, OK
// 	service: nombre de la app, OK
// 	traceId: "Id del request (%X{X-B3-TraceId:-}) ", NO hace falta
// 	spanId: "span generado dentro del request %X{X-B3-SpanId:-}", NO hace falta
// 	parentSpanId: "Id del span caller ("%X{X-B3-ParentSpanId:-}")", NO hace falta
// 	pid: "PID", OK
// 	thread_name: "thread en caso de gestionar threads", (No lavamos a poner)
// 	class: "class name with namespace", (falta ver como hacerlo)
// 	message: "mensaje a loguear" OK
//   }
