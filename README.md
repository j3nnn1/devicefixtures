# DeviceFixture

## intro ## 

This is a script in golang used to generate config files (jmeterThreadConfig.csv, jmeterThreadConfig.sql, jmeterThreadConfigRollback.sql) to insert in a database and make load test.

## how to use
```` 
go build .

go install.

./devicefixtures
````
And check this files:

* jmeterThreadConfig.csv
* jmeterThreadConfig.sql
* jmeterThreadConfigRollback.sql

### Tables 

```   	
SELECT * FROM dhcplogic.STATE_TAGS where serial_number='48575443C7D5639A';
SELECT * FROM dhcplogic.DEVICE_STATE where serial_number='48575443C7D5639A';
-- {"rack":"0","shelf":"0","slot":"15","port":"1","state":"OPERATING","clientID":"CLI-1002","clientName":"Rick Hunter","organizationCode":"TECOAR","VLAN_SP_PAIR":[{"serviceProvider":"OPERATING","vlanID":"3422"}]}
```
```
//dbcli.DB.Debug().Exec(`
//  insert into
//  STATE_TAGS (spid, st_name)
//  values (?,?)`,
//	sp.SERVICEPROVIDERID,
//	st.name,
//)
```